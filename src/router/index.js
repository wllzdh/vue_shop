import Vue from 'vue'
import Router from 'vue-router'
import Login from '../components/Login.vue'
import Home from '../components/Home.vue';



Vue.use(Router)

const router = new Router({
  routes:[
    {path:'/',redirect:'/login'}, //重定向
    {path:'/login',component:Login},
    {path:'/home',component:Home}
  ]
})

/**
 * 挂载路由导航守卫 
 * to表示将要访问的路径
 * from表示从哪个路径跳转而来，
 * next是一个函数，表示放行，是下一个要做的操作 next('/login')强制跳转login 
*/
router.beforeEach((to,from,next)=>{
  if(to.path ==='/login') return next()
  const tokenStr=window.sessionStorage.getItem('token');
  if(!tokenStr) return next('/login');
  next();
})


export default router
