import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './plugins/element.js' 
import './assets/fonts/icon/iconfont.css'// 导入字体图标
import './assets/css/global.css' // 导入全局样式


import axios from 'axios'
// 配置请求的根路径
axios.defaults.baseURL="https://www.fastmock.site/mock/64fbf1c1cd6b10577018f799a58cbf74/api"
Vue.prototype.$http=axios


Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
